package model.logic;

import API.IManager;
import controller.Controller.ResultadoCampanna;
import model.data_structures.ICola;
import model.data_structures.ILista;
import model.vo.Bike;
import model.vo.BikeRoute;
import model.vo.Station;
import model.vo.Trip;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Manager implements IManager {
	//Ruta del archivo de datos 2017-Q1
	// TODO Actualizar
	public static final String TRIPS_Q1 = "Ruta Trips 2017-Q1 en directorio data";
	
	//Ruta del archivo de trips 2017-Q2
	// TODO Actualizar	
	public static final String TRIPS_Q2 = "Ruta Trips 2017-Q2 en directorio data";
		
	//Ruta del archivo de trips 2017-Q3
	// TODO Actualizar	
	public static final String TRIPS_Q3 = "Ruta Trips 2017-Q3 en directorio data";
			
	//Ruta del archivo de trips 2017-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q4 = "Ruta Trips 2017-Q4 en directorio data";
		
	//Ruta del archivo de trips 2017-Q1-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q1_Q4 = "Ruta Trips 2017-Q1-Q4 en directorio data";

	//Ruta del archivo de stations 2017-Q1-Q2
	// TODO Actualizar	
	public static final String STATIONS_Q1_Q2 = "Ruta Stations 2017-Q1-Q2 en directorio data";

	//Ruta del archivo de stations 2017-Q3-Q4
	// TODO Actualizar	
	public static final String STATIONS_Q3_Q4 = "Ruta Stations 2017-Q3-Q4 en directorio data";
	
	//Ruta del archivo de stations 2017-Q3-Q4
	// TODO Actualizar	
	public static final String BIKE_ROUTES = "";
		
		
	@Override
	public void cargarDatos(String rutaTrips, String rutaStations, String dataBikeRoutes) {
		// TODO Auto-generated method stub
	}

	@Override
	public ICola<Trip> A1(int n, LocalDate fechaTerminacion) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<Trip> A2(int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<Trip> A3(int n, LocalDate fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<Bike> B1(int limiteInferior, int limiteSuperior) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<Trip> B2(String estacionDeInicio, String estacionDeLlegada, int limiteInferiorTiempo, int limiteSuperiorTiempo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] B3(String estacionDeInicio, String estacionDeLlegada) {
		// TODO Auto-generated method stub
		return new int[] {0, 0};
	}

	@Override
	public ResultadoCampanna C1(double valorPorPunto, int numEstacionesConPublicidad, int mesCampanna) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double[] C2(int LA, int LO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int darSector(double latitud, double longitud) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ILista<Station> C3(double latitud, double longitud) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<BikeRoute> C4(double latitud, double longitud) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<BikeRoute> C5(double latitudI, double longitudI, double latitudF, double longitudF){
		// TODO Auto-generated method stub
		return null;
	}

	
}
